package de.joshuanoeske.telegram.stillepostbot

import com.github.kotlintelegrambot.entities.BotCommand

class Commands {
    class Strings {
        companion object {
            const val START = "start"
            const val REGISTER = "register"
            const val UNREGISTER = "unregister"
            const val NEW_GAME = "newgame"
            const val PLAY = "play"
            const val TURNS = "turns"
            const val START_GAME = "startgame"
            const val STOP_GAME = "stopgame"
            const val GET_LAST_GAME = "getlastgame"
            const val GET_PLAYER_ORDER = "getplayerorder"
            const val HELP = "help"
        }
    }
    
    companion object {
        val START = BotCommand(Strings.START, "Startet den Bot.")
        val REGISTER = BotCommand(Strings.REGISTER, "Registriere dich für neue Spiele. Nur im privaten Chat verwendbar.")
        val UNREGISTER = BotCommand(Strings.UNREGISTER, "Lösche deine Registierung. Nur im privaten Chat verwendbar.")
        val NEW_GAME = BotCommand(Strings.NEW_GAME, "Erstelle ein neues Spiel.")
        val PLAY = BotCommand(Strings.PLAY, "Nehme am neuen Spiel teil.")
        val TURNS = BotCommand(Strings.TURNS, "Verändere die Anzahl der zu spielenden Runden.")
        val START_GAME = BotCommand(Strings.START_GAME, "Starte das Spiel.")
        val STOP_GAME = BotCommand(Strings.STOP_GAME, "Lösche das laufende Spiel.")
        val GET_LAST_GAME = BotCommand(Strings.GET_LAST_GAME, "Hol dir alle virtuellen Zettel des letzten Spiels.")
        val GET_PLAYER_ORDER = BotCommand(Strings.GET_PLAYER_ORDER, "Sieh dir die Reihenfolge der Spieler an.")
        val HELP = BotCommand(Strings.HELP, "Hilfe.")

        val commandList = listOf<BotCommand>(
            REGISTER,
            UNREGISTER,
            NEW_GAME,
            PLAY,
            TURNS,
            START_GAME,
            STOP_GAME,
            GET_LAST_GAME,
            GET_PLAYER_ORDER
        )
    }
}