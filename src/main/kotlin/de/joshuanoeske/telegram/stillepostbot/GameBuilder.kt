package de.joshuanoeske.telegram.stillepostbot

import kotlin.random.Random

class GameBuilder(val groupId: Long) {
    private val users: MutableList<Long> = mutableListOf()
    var turns: Int = 12

    val userCount: Int
        get() = users.size

    fun addUser(user: Long) {
        if (!users.contains(user)) {
            users.add(user)
        }
    }

    fun containsUser(user: Long) = users.contains(user)

    fun toGame(
        startGameMessage: (groupId: Long, userId: Long) -> Unit,
        noNewSheetAvailableMessage: (groupId: Long, userId: Long) -> Unit,
        nextSheetMessage: (groupId: Long, userId: Long, isQuestion: Boolean, message: String) -> Unit,
        gameOverMessage: (groupId: Long, userId: Long) -> Unit,
        allMessages: (groupId: Long, userId: Long, messages: List<String>) -> Unit,
        gameOverGroupMessage: (groupId: Long) -> Unit
    ): Game {
        return Game(groupId, users.sortedBy { Random.nextInt() }, turns, startGameMessage, noNewSheetAvailableMessage, nextSheetMessage, gameOverMessage, allMessages, gameOverGroupMessage)
    }
}