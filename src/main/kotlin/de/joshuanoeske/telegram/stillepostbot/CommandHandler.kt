package de.joshuanoeske.telegram.stillepostbot

import com.github.kotlintelegrambot.dispatcher.Dispatcher
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.dispatcher.text
import java.lang.StringBuilder
import kotlin.NumberFormatException

object CommandHandler {
    private val games: MutableMap<Long, Game> = mutableMapOf()

    private val gameBuilders: MutableMap<Long, GameBuilder> = mutableMapOf()

    init {
        UserManager
    }

    val dispatch = fun Dispatcher.() {
        command(Commands.START.command) { bot, update, _ ->
            val chatId = update.message?.chat?.id ?: return@command
            if ((update.message?.chat?.type == "private") && update.message?.from != null) {
                bot.sendMessage(chatId, Strings.getStartMessage(update.message?.from?.firstName ?: "Spieler"))
            } else {
                bot.sendMessage(chatId, Strings.PRIVATE_ONLY_COMMAND)
            }
        }

        command(Commands.REGISTER.command) { bot, update, _ ->
            val chatId = update.message?.chat?.id ?: return@command
            val userId = update.message?.from?.id ?: return@command
            if ((update.message?.chat?.type == "private") && update.message?.from != null) {
                if (!UserManager.containsUser(userId)) {
                    bot.sendMessage(chatId, Strings.NEW_REGISTER)
                } else {
                    bot.sendMessage(chatId, Strings.ALREADY_REGISTERED)
                }
                UserManager.setUser(userId, Pair(update.message!!.from!!, update.message!!.chat))
            } else {
                bot.sendMessage(chatId, Strings.PRIVATE_ONLY_COMMAND)
            }
        }

        command(Commands.UNREGISTER.command) { bot, update, _ ->
            val chatId = update.message?.chat?.id ?: return@command
            val userId = update.message?.from?.id ?: return@command
            if ((update.message?.chat?.type == "private") && update.message?.from != null) {
                if (UserManager.containsUser(userId)) {
                    if (!UserManager.isUserInGame(userId)) {
                        UserManager.removeUser(userId)
                        bot.sendMessage(chatId, Strings.DELETED)
                    } else {
                        bot.sendMessage(chatId, Strings.DELETION_NOT_POSSIBLE_IN_GAME)
                    }
                } else {
                    bot.sendMessage(chatId, Strings.NOT_REGISTERED)
                }
            } else {
                bot.sendMessage(chatId, Strings.PRIVATE_ONLY_COMMAND)
            }
        }

        command(Commands.NEW_GAME.command) { bot, update, _ ->
            val chatId = update.message?.chat?.id ?: return@command
            if ((update.message?.chat?.type == "group" || update.message?.chat?.type == "supergroup")) {
                if (!gameBuilders.containsKey(chatId)) {
                    val oldGame = games[chatId]
                    if (oldGame == null || oldGame.isGameFinished) {
                        games.remove(chatId)
                        gameBuilders[chatId] = GameBuilder(chatId)
                        bot.sendMessage(chatId, Strings.NEW_GAME_CREATED)
                    } else {
                        bot.sendMessage(chatId, Strings.GAME_ALREADY_RUNNING)
                    }
                } else {
                    bot.sendMessage(chatId, Strings.GAME_ALREADY_GETS_PREPARED)
                }
            } else {
                bot.sendMessage(chatId, Strings.GROUP_ONLY_COMMAND)
            }
        }

        command(Commands.PLAY.command) { bot, update, _ ->
            val chatId = update.message?.chat?.id ?: return@command
            val userId = update.message?.from?.id ?: return@command
            if ((update.message?.chat?.type == "group" || update.message?.chat?.type == "supergroup")) {
                val oldGame = games[chatId]
                if (oldGame == null) {
                    if (gameBuilders.containsKey(chatId)) {
                        if (UserManager.containsUser(userId)) {
                            if (!gameBuilders[chatId]!!.containsUser(userId)) {
                                if (!UserManager.isUserInGame(userId)) {
                                    UserManager.setUserInGame(userId, chatId)
                                    gameBuilders[chatId]!!.addUser(userId)
                                    bot.sendMessage(
                                        chatId,
                                        Strings.getUserTakesPartMessage(update.message!!.from!!.firstName)
                                    )
                                } else {
                                    bot.sendMessage(chatId, Strings.ALREADY_IN_GAME)
                                }
                            } else {
                                bot.sendMessage(chatId, Strings.ALREADY_IN_THIS_GAME)
                            }
                        } else {
                            bot.sendMessage(chatId, Strings.NOT_REGISTERED_YET)
                        }
                    } else {
                        bot.sendMessage(chatId, Strings.NO_GAME_PREPARING)
                    }
                } else {
                    if (oldGame.isGameFinished) {
                        bot.sendMessage(chatId, Strings.GAME_FINISHED_START_NEW)
                    } else {
                        bot.sendMessage(chatId, Strings.GAME_ALREADY_RUNNING)
                    }
                }
            } else {
                bot.sendMessage(chatId, Strings.GROUP_ONLY_COMMAND)
            }
        }

        command(Commands.TURNS.command) { bot, update, args ->
            val chatId = update.message?.chat?.id ?: return@command
            if ((update.message?.chat?.type == "group" || update.message?.chat?.type == "supergroup")) {
                val oldGame = games[chatId]
                if (oldGame == null) {
                    if (gameBuilders.containsKey(chatId)) {
                        if (args.size == 1) {
                            try {
                                val turns = args[0].toInt()
                                if (turns < 1) throw NumberFormatException("Not greater 0.")
                                gameBuilders[chatId]!!.turns = turns
                                bot.sendMessage(chatId, Strings.getNumberOfTurnsSetMessage(turns))
                            } catch (e: NumberFormatException) {
                                bot.sendMessage(chatId, Strings.NOT_AN_INTEGER)
                            }
                        } else {
                            bot.sendMessage(chatId, Strings.NOT_AN_INTEGER)
                        }
                    } else {
                        bot.sendMessage(chatId, Strings.NO_GAME_PREPARING)
                    }
                } else {
                    if (oldGame.isGameFinished) {
                        bot.sendMessage(chatId, Strings.GAME_FINISHED_START_NEW)
                    } else {
                        bot.sendMessage(chatId, Strings.GAME_ALREADY_RUNNING)
                    }
                }
            } else {
                bot.sendMessage(chatId, Strings.GROUP_ONLY_COMMAND)
            }
        }

        command(Commands.START_GAME.command) { bot, update, _ ->
            val chatId = update.message?.chat?.id ?: return@command
            if ((update.message?.chat?.type == "group" || update.message?.chat?.type == "supergroup")) {
                val oldGame = games[chatId]
                if (oldGame == null) {
                    if (gameBuilders.containsKey(chatId)) {
                        if (gameBuilders[chatId]!!.userCount > 2) {
                            games[chatId] = gameBuilders[chatId]!!.toGame({ _, userId ->
                                bot.sendMessage(
                                    UserManager.getUser(userId)!!.second.id,
                                    Strings.NEW_GAME_WELCOME_MESSAGE
                                )
                            }, { _, userId ->
                                bot.sendMessage(
                                    UserManager.getUser(userId)!!.second.id,
                                    Strings.NO_NEW_SHEET_AVALIABLE
                                )
                            }, { _, userId, isQuestion, message ->
                                bot.sendMessage(
                                    UserManager.getUser(userId)!!.second.id,
                                    if (isQuestion) Strings.WRITE_ANSWER else Strings.WRITE_QUESTION
                                )
                                bot.sendMessage(
                                    UserManager.getUser(userId)!!.second.id,
                                    message
                                )
                            }, { _, userId ->
                                bot.sendMessage(
                                    UserManager.getUser(userId)!!.second.id,
                                    Strings.GAME_OVER_PRIVATE
                                )
                            }, { _, userId, messages ->
                                bot.sendMessage(
                                    UserManager.getUser(userId)!!.second.id,
                                    Strings.ALL_MESSAGES
                                )
                                for (message in messages) {
                                    bot.sendMessage(
                                        UserManager.getUser(userId)!!.second.id,
                                        message
                                    )
                                }
                            }, { groupId ->
                                bot.sendMessage(groupId, Strings.GAME_OVER_GROUP)
                                UserManager.removeGroupInGame(groupId)
                            })
                            gameBuilders.remove(chatId)
                            games[chatId]!!.startGame()
                            bot.sendMessage(chatId, Strings.GAME_HAS_BEGUN)
                        } else {
                            bot.sendMessage(chatId, Strings.NOT_ENOUGH_PLAYERS)
                        }
                    } else {
                        bot.sendMessage(chatId, Strings.NO_GAME_PREPARING)
                    }
                } else {
                    if (oldGame.isGameFinished) {
                        bot.sendMessage(chatId, Strings.GAME_FINISHED_START_NEW)
                    } else {
                        bot.sendMessage(chatId, Strings.GAME_ALREADY_RUNNING)
                    }
                }
            } else {
                bot.sendMessage(chatId, Strings.GROUP_ONLY_COMMAND)
            }
        }

        text { _, update ->
            val userId = update.message?.from?.id ?: return@text
            if (update.message?.chat?.type == "private") {
                val currentGame = UserManager.getCurrentGameOfUser(userId)
                if (currentGame != null && games[currentGame] != null) {
                    if (update.message?.text != null) {
                        games[currentGame]!!.receivedMessage(userId, update.message!!.text!!)
                    }
                }
            }
        }

        command(Commands.STOP_GAME.command) { bot, update, _ ->
            val chatId = update.message?.chat?.id ?: return@command
            if ((update.message?.chat?.type == "group" || update.message?.chat?.type == "supergroup")) {
                when {
                    games.containsKey(chatId) -> {
                        if (games[chatId]!!.isGameFinished) {
                            bot.sendMessage(chatId, Strings.FINISHED_GAME_REMOVED)
                        } else {
                            bot.sendMessage(chatId, Strings.RUNNING_GAME_REMOVED)
                            UserManager.removeGroupInGame(chatId)
                        }
                        games.remove(chatId)
                    }
                    gameBuilders.containsKey(chatId) -> {
                        gameBuilders.remove(chatId)
                        UserManager.removeGroupInGame(chatId)
                        bot.sendMessage(chatId, Strings.PREPARING_GAME_REMOVED)
                    }
                    else -> {
                        bot.sendMessage(chatId, Strings.NO_GAME_EXISTS)
                    }
                }
            } else {
                bot.sendMessage(chatId, Strings.GROUP_ONLY_COMMAND)
            }
        }

        command(Commands.GET_PLAYER_ORDER.command) { bot, update, _ ->
            val chatId = update.message?.chat?.id ?: return@command
            if ((update.message?.chat?.type == "group" || update.message?.chat?.type == "supergroup")) {
                val game = games[chatId]
                if (game != null) {
                    bot.sendMessage(chatId, Strings.PLAYER_ORDER)
                    val playerOrder = StringBuilder()
                    for (user in game.users) {
                        playerOrder.append(UserManager.getUser(user)?.first?.firstName ?: Strings.UNKNOWN_USER).append(" → ")
                    }
                    playerOrder.append(UserManager.getUser(game.users.first())?.first?.firstName ?: Strings.UNKNOWN_USER)
                    bot.sendMessage(chatId, playerOrder.toString())
                } else {
                    bot.sendMessage(chatId, Strings.NO_RUNNING_OR_FINISHED_GAME)
                }
            } else {
                bot.sendMessage(chatId, Strings.GROUP_ONLY_COMMAND)
            }
        }

        command(Commands.GET_LAST_GAME.command) { bot, update, _ ->
            val chatId = update.message?.chat?.id ?: return@command
            if ((update.message?.chat?.type == "group" || update.message?.chat?.type == "supergroup")) {
                val game = games[chatId]
                if (game != null && game.isGameFinished) {
                    val sheets = game.getAllSheets()
                    bot.sendMessage(chatId, Strings.ALL_MESSAGES_GROUP)
                    sheets.forEachIndexed { index, list ->
                        bot.sendMessage(chatId, Strings.getSheetInList(index))
                        for (message in list) {
                            bot.sendMessage(chatId, message)
                        }
                    }
                } else {
                    bot.sendMessage(chatId, Strings.NO_FINISHED_GAME)
                }
            } else {
                bot.sendMessage(chatId, Strings.GROUP_ONLY_COMMAND)
            }
        }
    }
}