package de.joshuanoeske.telegram.stillepostbot

import kotlin.random.Random

class Strings {
    companion object {
        fun getStartMessage(name: String): String {
            return "Hallo $name. Wenn du dich registrieren möchtest, um danach spielen zu können, sende mir den Befehl /${Commands.Strings.REGISTER}."
        }

        const val NEW_REGISTER = "Du bist nun registriert."
        const val ALREADY_REGISTERED = "Du warst bereits registriert."
        const val NOT_REGISTERED = "Du bist nicht registiert."
        const val DELETION_NOT_POSSIBLE_IN_GAME =
            "Du befindest dich gerade in einem Spiel, daher kann ich deine Registrierung nicht löschen."
        const val DELETED =
            "Ich habe deine Registierung gelöscht. Wenn du erneut spielen möchtest, sende mir einfach wieder /${Commands.Strings.REGISTER}."

        const val NEW_GAME_CREATED =
            "Ich habe ein neues Spiel erstellt. Alle, die daran teilnehmen wollen, müssen /${Commands.Strings.PLAY} schreiben. " +
                    "Dazu muss man jedoch vorher im privaten Chat mit mir einmal /${Commands.Strings.REGISTER} schreiben.\n" +
                    "Mit /${Commands.Strings.TURNS} kannst man die Anzahl der Runden, die ihr spielen wollt, verändern. " +
                    "Mit /${Commands.Strings.START_GAME} startet ihr das Spiel."
        const val GAME_ALREADY_EXISTS =
            "Es läuft bereits ein Spiel oder es wird bereits gerade ein Spiel erstellt. Um das aktuelle Spiel zu beenden, sende /${Commands.Strings.STOP_GAME}."
        const val GAME_ALREADY_RUNNING =
            "Es läuft bereits ein Spiel. Um das aktuelle Spiel zu beenden, sende /${Commands.Strings.STOP_GAME}."
        const val GAME_ALREADY_GETS_PREPARED =
            "Es wird bereits ein Spiel vorbereitet. Um dieses Spiel zu löschen, sende /${Commands.Strings.STOP_GAME}."
        const val NO_GAME_PREPARING =
            "Es befindet sich derzeit kein Spiel in Vorbereitung."
        const val NOT_REGISTERED_YET =
            "Du bist noch nicht registriert. Du musst zunächst den privaten Chat mit mir starten und dort /${Commands.Strings.REGISTER} schreiben."
        const val NOT_AN_INTEGER =
            "Es muss genau eine positive Ganzzahl hinter /${Commands.Strings.TURNS} angegeben werden."
        const val ALREADY_IN_GAME =
            "Du bist bereits in einem Spiel. Derzeit kann man nur an einem Spiel simultan teilnehmen. Beende bitte das vorherige Spiel."
        const val ALREADY_IN_THIS_GAME = "Du bist bereits für dieses Spiel registiert."
        const val NOT_ENOUGH_PLAYERS =
            "Es haben sich bisher zu wenig Personen gemeldet, die mitspielen wollen. Es sollten mindestens drei Personen mitspielen."
        const val GAME_HAS_BEGUN =
            "Das Spiel hat begonnen. Alle, die mitspielen, haben nun eine private Nachricht erhalten."
        const val NEW_GAME_WELCOME_MESSAGE =
            "Hallo! Du bist Teil dieses neuen Spiels. " +
                    "Sende mir bitte während des Spiels im Privatchat keine Kommandos, da diese als Eingaben für das Spiel gewertet werden könnten.\n" +
                    "Bitte gib nun zu Beginn eine Frage ein und sende sie ab."
        const val NO_NEW_SHEET_AVALIABLE =
            "Es gibt im Moment keine neue Frage/Antwort für dich. Die Person vor dir ist offenbar ziemlich langsam (oder du bist zu schnell)."
        const val WRITE_ANSWER = "Bitte schreibe eine Antwort auf die folgende Frage:"
        const val WRITE_QUESTION =
            "Bitte schreibe eine Frage, sodass man diese mit der folgenden Antwort beantworten kann:"
        const val GAME_OVER_PRIVATE =
            "Dein Zettel ist jetzt fertig. Bitte warte, bis die anderen ebenfalls fertig sind."
        const val ALL_MESSAGES =
            "Alle haben ihre Zettel fertiggestellt. Im Folgenden erhälst du alle Frage-Antwort-Paare des Zettels, den du zuletzt bearbeitet hast."
        const val GAME_OVER_GROUP = "Das Spiel ist jetzt beendet! Ihr könnt jetzt eure virtuellen Zettel vorlesen."
        const val NO_GAME_EXISTS = "Es existiert derzeit kein Spiel."
        const val RUNNING_GAME_REMOVED = "Ich habe das laufende Spiel gelöscht."
        const val FINISHED_GAME_REMOVED = "Ich habe das abgeschlossene Spiel gelöscht."
        const val PREPARING_GAME_REMOVED = "Ich habe das Spiel, das sich in Vorbereitung befand, gelöscht."
        const val NO_RUNNING_GAME = "Es existiert im Moment kein laufendes Spiel."
        const val NO_RUNNING_OR_FINISHED_GAME = "Es existiert derzeit kein laufendes oder abgeschlossenes Spiel."
        const val NO_FINISHED_GAME = "Es ist kein abgeschlossenes Spiel gespeichert."
        const val ALL_MESSAGES_GROUP = "Ich sende euch nun alle Zettel des vorherigen Spiels."
        const val PLAYER_ORDER = "Die Reihenfolge der Mitspielenden ist wie folgt:"
        const val GAME_FINISHED_START_NEW =
            "Das bisherige Spiel ist beendet. Wenn du ein neues starten möchtest, verwende /${Commands.Strings.NEW_GAME}."
        const val UNKNOWN_USER = "Unbekannter Nutzer"

        fun getSheetInList(number: Int): String {
            return "Nun folgt Zettel $number."
        }

        fun getUserTakesPartMessage(name: String): String {
            return listOf<String>("Sehr schön, $name spielt mit.", "$name spielt mit, wie schön.", "Hurra, $name spielt mit!").let { it[Random.nextInt(it.size)] }
        }

        fun getNumberOfTurnsSetMessage(number: Int): String {
            return "Die Anzahl der zu spielenden Runden wurde auf $number gesetzt."
        }

        const val GROUP_ONLY_COMMAND = "Diesen Befehl kannst du nur in einer Gruppe verwenden."
        const val PRIVATE_ONLY_COMMAND = "Diesen Befehl kannst du nur im privaten Chat mit mir verwenden."
    }
}