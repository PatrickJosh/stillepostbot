package de.joshuanoeske.telegram.stillepostbot

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.github.kotlintelegrambot.entities.Chat
import com.github.kotlintelegrambot.entities.User
import java.io.File
import java.io.IOException
import kotlin.system.measureTimeMillis

object UserManager {
    private val users: MutableMap<Long, Pair<User, Chat>>

    //First is userId, second is groupId
    private val usersInGame: MutableSet<Pair<Long, Long>> = mutableSetOf()

    private val mapper = ObjectMapper().registerKotlinModule()

    init {
        val timeBefore = System.currentTimeMillis()
        println("UserManager: Init.")
        val usersData = File("./data/users.json")
        users = if (usersData.exists() && usersData.isFile) {
            try {
                mapper.readValue(usersData, object : TypeReference<MutableMap<Long, Pair<User, Chat>>>() {})
            } catch (e: IOException) {
                println("UserManager: Error while reading file.")
                e.printStackTrace()
                mutableMapOf<Long, Pair<User, Chat>>()
            } catch (e: JsonParseException) {
                println("UserManager: File is not a json.")
                e.printStackTrace()
                mutableMapOf<Long, Pair<User, Chat>>()
            }
        } else {
            println("UserManager: File not found.")
            mutableMapOf<Long, Pair<User, Chat>>()
        }
        println("UserManager: Initialisation finished.")
        println("UserManager: Initialisation took ${System.currentTimeMillis() - timeBefore} ms.")
    }

    fun setUser(key: Long, user: Pair<User, Chat>) {
        if (users[key] != user) {
            users[key] = user
            updateUserJson()
        }
    }

    fun removeUser(key: Long) {
        if (users.remove(key) != null) {
            updateUserJson()
        }
    }

    private fun updateUserJson() {
        val time = measureTimeMillis {
            println("UserManager: Update JSON.")
            val directory = File("./data")
            if (!directory.exists()) {
                directory.mkdir()
            }
            val file = File("./data/users.json")
            if (file.exists()) {
                file.delete()
            }
            mapper.writeValue(file, users)
            println("UserManager: Update JSON finished.")
        }
        println("UserManager: Update of JSON took $time ms.")
    }

    fun containsUser(key: Long) = users.containsKey(key)

    fun getUser(key: Long) = users[key]

    fun setUserInGame(userId: Long, groupId: Long) {
        if (users.containsKey(userId) && !isUserInGame(userId)) {
            usersInGame.add(Pair(userId, groupId))
        }
    }

    fun removeUserInGame(userId: Long) {
        val iterator = usersInGame.iterator()
        while (iterator.hasNext()) {
            if (iterator.next().first == userId) {
                iterator.remove()
                return
            }
        }
    }

    fun removeGroupInGame(groupId: Long) {
        val iterator = usersInGame.iterator()
        while (iterator.hasNext()) {
            if (iterator.next().second == groupId) {
                iterator.remove()
            }
        }
    }

    fun isUserInGame(key: Long): Boolean {
        for (element in usersInGame) {
            if (element.first == key) {
                return true
            }
        }
        return false
    }

    fun getCurrentGameOfUser(key: Long): Long? {
        for (element in usersInGame) {
            if (element.first == key) {
                return element.second
            }
        }
        return null
    }
}