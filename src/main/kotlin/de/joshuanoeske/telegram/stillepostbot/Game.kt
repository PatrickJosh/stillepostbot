package de.joshuanoeske.telegram.stillepostbot

import java.lang.StringBuilder
import java.util.*

class Game(
    val groupId: Long,
    val users: List<Long>,
    val turns: Int,
    private val startGameMessage: (groupId: Long, userId: Long) -> Unit,
    private val noNewSheetAvailableMessage: (groupId: Long, userId: Long) -> Unit,
    private val nextSheetMessage: (groupId: Long, userId: Long, isQuestion: Boolean, message: String) -> Unit,
    private val gameOverMessage: (groupId: Long, userId: Long) -> Unit,
    private val allMessages: (groupId: Long, userId: Long, messages: List<String>) -> Unit,
    private val gameOverGroupMessage: (groupId: Long) -> Unit
) {
    val sheets: List<MutableList<String>> = List(users.size) { mutableListOf<String>() }
    val players: Map<Long, Player>
    private val turnsInternal = turns * 2
    val playerCount: Int
        get() = users.size
    var finishedPlayerCount: Int = 0
        private set
    val isGameFinished: Boolean
        get() = playerCount == finishedPlayerCount

    init {
        val tempPlayers: MutableMap<Long, Player> = mutableMapOf()
        users.forEachIndexed { index, element ->
            tempPlayers[element] = Player(element, index, this::newSheetNotifier)
        }
        players = tempPlayers.toMap()
    }

    fun startGame() {
        for (user in users) {
            startGameMessage(groupId, user)
        }
    }

    fun receivedMessage(userId: Long, message: String) {
        val player = players[userId]
        if (player != null) {
            if (player.currentSheet != null) {
                sheets[player.currentSheet!!].add(message)
                val oldSheet = player.removeCurrentSheet()!!
                if (player.turns < turnsInternal) {
                    players[users[(player.positionInList + 1) % users.size]]?.addNextSheet(oldSheet) //TODO: ?: Error
                    if (player.remainingElements > 0) {
                        nextSheetMessage(
                            groupId,
                            player.id,
                            sheets[player.currentSheet!!].lastIndex % 2 == 0,
                            sheets[player.currentSheet!!].last()
                        )
                    } else {
                        noNewSheetAvailableMessage(groupId, player.id)
                    }
                } else {
                    gameOverMessage(groupId, userId)
                    finishedPlayerCount++
                    if (finishedPlayerCount == users.size) {
                        gameOver()
                    }
                }

            }
        } /*else {
            //TODO: Error
        }*/
    }

    fun getAllSheets(): List<List<String>> {
        val sheetList: MutableList<List<String>> = mutableListOf()
        for (i in sheets.indices) {
            sheetList.add(getSheet(i))
        }
        return sheetList.toList()
    }

    private fun gameOver() {
        gameOverGroupMessage(groupId)
        for (userId in users) {
            val player = players[userId]
            if (player != null) {
                allMessages(groupId, userId, getSheet(player.lastSheet))
            }
        }
    }

    private fun getSheet(sheetIndex: Int): List<String> {
        val messages: MutableList<String> = mutableListOf()
        val sheet = sheets[sheetIndex]
        val sheetBuilder = StringBuilder(turnsInternal)
        for (message in sheet) {
            if (sheetBuilder.length + message.length > 4000) {
                messages.add(sheetBuilder.toString())
                sheetBuilder.clear()
            }
            sheetBuilder.append("$message\n")
        }
        messages.add(sheetBuilder.toString())
        return messages.toList()
    }


    private fun newSheetNotifier(userId: Long, remainingElements: Int) {
        if (remainingElements == 1) {
            val player = players[userId]
            if (player != null) {
                nextSheetMessage(
                    groupId, userId, sheets[player.currentSheet!!].lastIndex % 2 == 0,
                    sheets[player.currentSheet!!].last()
                )
            }
        }
    }

    class Player(
        val id: Long,
        val positionInList: Int,
        val newSheetNotifier: (userId: Long, remainingElements: Int) -> Unit
    ) {
        var turns: Int = 0
            private set
        private val nextSheets: Queue<Int> = ArrayDeque<Int>().apply { add(positionInList) }
        var lastSheet: Int = positionInList
            private set

        val currentSheet: Int?
            get() = nextSheets.peek()
        val remainingElements: Int
            get() = nextSheets.size

        fun addNextSheet(sheet: Int) {
            nextSheets.add(sheet)
            newSheetNotifier(id, nextSheets.size)
        }

        fun removeCurrentSheet(): Int? {
            turns++
            lastSheet = nextSheets.poll()
            return lastSheet
        }
    }
}