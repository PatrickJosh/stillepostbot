package de.joshuanoeske.telegram.stillepostbot

import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.webhook
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.applicationEngineEnvironment
import io.ktor.server.engine.connector
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import java.io.File

fun main() {
    val bot = bot {
        token = BotConfig.API_TOKEN
        webhook {
            url = "${BotConfig.HOSTNAME}/${BotConfig.API_TOKEN}"
            allowedUpdates = listOf("messages")
        }
        dispatch(CommandHandler.dispatch)
    }
    bot.startWebhook()
    bot.setMyCommands(Commands.commandList)

    val env = applicationEngineEnvironment {
        module {
            routing {
                post("/${BotConfig.API_TOKEN}") {
                    println("test")
                    val response = call.receiveText()
                    println(response)
                    bot.processUpdate(response)
                    call.respond(HttpStatusCode.OK)
                }
            }
        }
        connector {
            host = "0.0.0.0"
            port = BotConfig.PORT
        }
    }

    embeddedServer(Netty, env).start(wait = true)
}

object BotConfig {
    val API_TOKEN: String
    val HOSTNAME: String
    val PORT: Int

    init {
        var tempApiToken = ""
        var tempHostname = ""
        var tempPort = 0
        File("./stillepostbot.conf").forEachLine {
            when {
                it.contains("TOKEN =", false) -> {
                    tempApiToken = it.split(" = ")[1]
                }
                it.contains("HOSTNAME =", false) -> {
                    tempHostname = it.split(" = ")[1]
                }
                it.contains("PORT =", false) -> {
                    tempPort = it.split(" = ")[1].toInt()
                }
            }
        }
        API_TOKEN = tempApiToken
        HOSTNAME = tempHostname
        PORT = tempPort
    }

}