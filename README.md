# Stille Post Bot

The "Stille Post Bot" is a Telegram bot which use can use to play the following game:
Some people (at least three) sit in a circle, everyone has a piece of paper and a pen.
First, everyone writes a question on top of that piece.
After they have done this, they give the sheet to the next person in the circle.
This person has to answer the question written on the paper.
After that, the person who answered the question folds the sheet so that you are not able to read the question anymore and passes the sheet on to the next person.
Now this person has to write a question that you could answer with the answer that is written on the paper.
Again, the person folds the sheet and hands it to the next person.
This sequence goes on and on until you decide to end the game or there is no more space on the sheet.
Then everyone reads the last sheet they got out.

### Further information

The bot currently supports only German.
It is proposed to translate the strings.

### Libraries

This project uses the following libraries or frameworks:

- [Kotlin Telegram Bot](https://github.com/kotlin-telegram-bot/kotlin-telegram-bot): Licenced under the Apache 2.0 license.
- [Ktor](https://github.com/ktorio/ktor): Licensed under the Apache 2.0 license.
- JacksonXML
  - [Jackson Databind](https://github.com/FasterXML/jackson-databind): Licensed under the Apache 2.0 license.
  - [Jackson Kotlin Module](https://github.com/FasterXML/jackson-module-kotlin): Licensed under the Apache 2.0 license.
